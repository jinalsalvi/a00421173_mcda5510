package com.sum.assignment3;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SimpleCsvParser {
	public static int validCounter = 0;
	public static int invalidCounter = 0;
	private static final String FILENAME = "C:\\Users\\Jinal\\git\\mscda_5510_samples\\JavaSamples\\data.txt";
	public void main(String args) {
		final long startTime = System.currentTimeMillis();
		BufferedWriter bw = null;
		FileWriter fw = null;
		Reader in;
		
		 try{    
	           fw=new FileWriter(FILENAME,true); 
	           bw = new BufferedWriter(fw);
	     }catch(Exception e){
	    	 System.out.println(e);
	     }    
	     
		try {
			in = new FileReader(args);
			
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			int ind = 0;
			for (CSVRecord record : records) {
				  
				if (ind == 0) {
					ind++;
					continue;
				} 
				
			  try {  
			  String fanme = record.get(0);
			  String lname = record.get(1);
			  String streetNo = record.get(2);
			  String street = record.get(3);
			  String city = record.get(4);
			  String province = record.get(5);
			  String postal = record.get(6);
			  String country = record.get(7);
			  String phone = record.get(8);
			  String email = record.get(9);
			  
					  
			  if(record.size() < 10) {
				  throw new Exception();  
			  }
			  
			  if(fanme==null || fanme.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }					  
			  else if(lname==null || lname.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else if(streetNo==null || streetNo.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else if(street==null || street.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else  if(city==null || city.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else  if(province==null || province.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  
			  else if(postal==null || postal.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else if(country==null || country.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else if(phone==null || phone.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  else if(email==null || email.equalsIgnoreCase(""))
			  {
				  throw new Exception();
			  }
			  validCounter++;

			  fw.append(fanme+",");
			  fw.append(lname+",");
			  fw.append(streetNo+",");
			  fw.append(street+",");
			  fw.append(city+",");
			  fw.append(province+",");
			  fw.append(postal+",");
			  fw.append(country+",");
			  fw.append(phone+",");
			  fw.append(email+"\n");      
			  }catch( Exception  e) {
				  
				  invalidCounter++;
				  
			  }
			}	
			final long endTime = System.currentTimeMillis();
			 fw.close();    
	    } catch ( IOException e) {
			e.printStackTrace();
		}

		
		
	}

}
