package com.sum.assignment3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

public class DirWalker {
	static String FILENAME = "C:\\Users\\Jinal\\git\\mscda_5510_samples\\JavaSamples\\log.txt";
	static BufferedWriter bw = null;
	static FileWriter fw1 = null;
	static Long excecutionTime;
	static int i;
	static int v;
    public void walk( String path ) {
    	final long startTime = System.currentTimeMillis();
    	SimpleCsvParser sp= new SimpleCsvParser();
         
        File root = new File( path );
        File[] list = root.listFiles();
    	
	    
    

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
            	
            	sp.main(f.getAbsoluteFile().toString());
                System.out.println( "File:" + f.getAbsoluteFile() );
            }
        }
        System.out.println( "Valid Count:" +sp.validCounter+" "+"Invalid Count: "+sp.invalidCounter );
    	final long endTime = System.currentTimeMillis();
    
    	excecutionTime = (endTime - startTime);
    	v = sp.validCounter;
    	i = sp.invalidCounter;
        	
    }

    public static void main(String[] args) {
    	DirWalker fw = new DirWalker();
   	 try{    
         fw1=new FileWriter(FILENAME,true); 
         bw = new BufferedWriter(fw1);
   }catch(Exception e){
  	 System.out.println(e);
   }    

    	fw.walk("C:\\Users\\Jinal\\Documents\\Source\\Repos\\A00421173_MCDA5510\\Assignment3\\Sample Data" );
    
        try {
			fw1.append("Valid Records: "+v);
		 	  fw1.append("\nInValid Records: "+i);
	    	  fw1.append("\nExecution Time: "+ excecutionTime +" ms");
	    	  fw1.close(); 
	   		} catch (IOException e) {
	   		// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}