﻿namespace Assignment2
{
    partial class ContactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1fn = new System.Windows.Forms.Label();
            this.textBoxFN = new System.Windows.Forms.TextBox();
            this.textBoxLN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCOUNTRY = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxP = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxEA = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_previous = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.importL = new System.Windows.Forms.Label();
            this.textImportFile = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.ButtonImport = new System.Windows.Forms.Button();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.textBoxError = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1fn
            // 
            this.label1fn.AutoSize = true;
            this.label1fn.Location = new System.Drawing.Point(36, 47);
            this.label1fn.Name = "label1fn";
            this.label1fn.Size = new System.Drawing.Size(76, 17);
            this.label1fn.TabIndex = 0;
            this.label1fn.Text = "First Name";
            this.label1fn.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxFN
            // 
            this.textBoxFN.Location = new System.Drawing.Point(147, 44);
            this.textBoxFN.Name = "textBoxFN";
            this.textBoxFN.Size = new System.Drawing.Size(168, 22);
            this.textBoxFN.TabIndex = 1;
            // 
            // textBoxLN
            // 
            this.textBoxLN.Location = new System.Drawing.Point(148, 74);
            this.textBoxLN.Name = "textBoxLN";
            this.textBoxLN.Size = new System.Drawing.Size(168, 22);
            this.textBoxLN.TabIndex = 3;
            this.textBoxLN.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Last Name";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBoxSN
            // 
            this.textBoxSN.Location = new System.Drawing.Point(148, 105);
            this.textBoxSN.Name = "textBoxSN";
            this.textBoxSN.Size = new System.Drawing.Size(168, 22);
            this.textBoxSN.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Street Number";
            // 
            // textBoxC
            // 
            this.textBoxC.Location = new System.Drawing.Point(148, 162);
            this.textBoxC.Name = "textBoxC";
            this.textBoxC.Size = new System.Drawing.Size(168, 22);
            this.textBoxC.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "City ";
            // 
            // textBoxPN
            // 
            this.textBoxPN.Location = new System.Drawing.Point(149, 280);
            this.textBoxPN.Name = "textBoxPN";
            this.textBoxPN.Size = new System.Drawing.Size(168, 22);
            this.textBoxPN.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "Phone Number";
            // 
            // textBoxPC
            // 
            this.textBoxPC.Location = new System.Drawing.Point(149, 252);
            this.textBoxPC.Name = "textBoxPC";
            this.textBoxPC.Size = new System.Drawing.Size(168, 22);
            this.textBoxPC.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Postal Code";
            // 
            // textBoxCOUNTRY
            // 
            this.textBoxCOUNTRY.Location = new System.Drawing.Point(149, 221);
            this.textBoxCOUNTRY.Name = "textBoxCOUNTRY";
            this.textBoxCOUNTRY.Size = new System.Drawing.Size(168, 22);
            this.textBoxCOUNTRY.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Country ";
            // 
            // textBoxP
            // 
            this.textBoxP.Location = new System.Drawing.Point(148, 191);
            this.textBoxP.Name = "textBoxP";
            this.textBoxP.Size = new System.Drawing.Size(168, 22);
            this.textBoxP.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Province ";
            // 
            // textBoxEA
            // 
            this.textBoxEA.Location = new System.Drawing.Point(150, 309);
            this.textBoxEA.Name = "textBoxEA";
            this.textBoxEA.Size = new System.Drawing.Size(168, 22);
            this.textBoxEA.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 312);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "email Address";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(147, 17);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(168, 22);
            this.textBoxID.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "ID";
            // 
            // button_previous
            // 
            this.button_previous.Location = new System.Drawing.Point(43, 375);
            this.button_previous.Name = "button_previous";
            this.button_previous.Size = new System.Drawing.Size(98, 39);
            this.button_previous.TabIndex = 20;
            this.button_previous.Text = "Previous";
            this.button_previous.UseVisualStyleBackColor = true;
            this.button_previous.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_next
            // 
            this.button_next.Location = new System.Drawing.Point(164, 375);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(108, 39);
            this.button_next.TabIndex = 21;
            this.button_next.Text = "Next";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button2_Click);
            // 
            // importL
            // 
            this.importL.AutoSize = true;
            this.importL.Location = new System.Drawing.Point(40, 438);
            this.importL.Name = "importL";
            this.importL.Size = new System.Drawing.Size(104, 17);
            this.importL.TabIndex = 22;
            this.importL.Text = "Import CSV File";
            // 
            // textImportFile
            // 
            this.textImportFile.Location = new System.Drawing.Point(164, 435);
            this.textImportFile.Name = "textImportFile";
            this.textImportFile.Size = new System.Drawing.Size(246, 22);
            this.textImportFile.TabIndex = 23;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(433, 431);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(82, 31);
            this.buttonBrowse.TabIndex = 24;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // ButtonImport
            // 
            this.ButtonImport.Location = new System.Drawing.Point(164, 475);
            this.ButtonImport.Name = "ButtonImport";
            this.ButtonImport.Size = new System.Drawing.Size(97, 33);
            this.ButtonImport.TabIndex = 25;
            this.ButtonImport.Text = "Import";
            this.ButtonImport.UseVisualStyleBackColor = true;
            this.ButtonImport.Click += new System.EventHandler(this.ButtonImport_Click);
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(148, 135);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(168, 22);
            this.textBoxAddress.TabIndex = 27;
            this.textBoxAddress.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(37, 138);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(60, 17);
            this.labelAddress.TabIndex = 26;
            this.labelAddress.Text = "Address";
            this.labelAddress.Click += new System.EventHandler(this.label10_Click);
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Location = new System.Drawing.Point(40, 342);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(40, 17);
            this.labelError.TabIndex = 28;
            this.labelError.Text = "Error";
            this.labelError.Click += new System.EventHandler(this.labelError_Click);
            // 
            // textBoxError
            // 
            this.textBoxError.Location = new System.Drawing.Point(150, 337);
            this.textBoxError.Name = "textBoxError";
            this.textBoxError.Size = new System.Drawing.Size(168, 22);
            this.textBoxError.TabIndex = 29;
            // 
            // ContactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 546);
            this.Controls.Add(this.textBoxError);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.textBoxAddress);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.ButtonImport);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.textImportFile);
            this.Controls.Add(this.importL);
            this.Controls.Add(this.button_next);
            this.Controls.Add(this.button_previous);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxEA);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxPN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxCOUNTRY);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxP);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSN);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxLN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxFN);
            this.Controls.Add(this.label1fn);
            this.Name = "ContactForm";
            this.Text = "Contact Form";
            this.Load += new System.EventHandler(this.ContactForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1fn;
        private System.Windows.Forms.TextBox textBoxFN;
        private System.Windows.Forms.TextBox textBoxLN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCOUNTRY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxEA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_previous;
        private System.Windows.Forms.Button button_next;
        private System.Windows.Forms.Label importL;
        private System.Windows.Forms.TextBox textImportFile;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Button ButtonImport;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.TextBox textBoxError;
    }
}

