USE [master]
GO
/****** Object:  Database [Customer]    Script Date: 2018-02-12 9:26:01 PM ******/
CREATE DATABASE [Customer] ON  PRIMARY 
( NAME = N'Customer', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Customer.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Customer_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Customer_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Customer] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Customer].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Customer] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Customer] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Customer] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Customer] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Customer] SET ARITHABORT OFF 
GO
ALTER DATABASE [Customer] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Customer] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Customer] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Customer] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Customer] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Customer] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Customer] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Customer] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Customer] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Customer] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Customer] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Customer] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Customer] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Customer] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Customer] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Customer] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Customer] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Customer] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Customer] SET  MULTI_USER 
GO
ALTER DATABASE [Customer] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Customer] SET DB_CHAINING OFF 
GO
USE [Customer]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 2018-02-12 9:26:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[streetNumber] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[province] [varchar](50) NULL,
	[country] [varchar](50) NULL,
	[postalCode] [varchar](50) NULL,
	[phoneNumber] [varchar](50) NULL,
	[emailAddress] [varchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (301, N'Vivek', N'Singh', N'590 Franklyn street', N'a', N'Torronto', N'British Columbia', N'Canada', N'B1H7K7', N'254-852-2578', N'Vivek.Singh@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (302, N'Karthik', N'Devaraj', N'890 Quinpool street', N'b', N'Vancouver', N'New Brunswick', N'Canada', N'B9H6J5', N'254-568-2125', N'Karthik.Devaraj@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (303, N'Naveenraj', N'Salvi', N'590 Franklyn street', N'c', N'Torronto', N'Alberta', N'Canada', N'B9H6J5', N'254-852-2578', N'Naveenraj.Salvi@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (304, N'Yifan', N'Hameed', N'660 Inglis Street', N'd', N'Brampton', N'New Brunswick', N'Canada', N'B3H2C9', N'254-852-2578', N'Yifan.Hameed@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (305, N'Yifan', N'Singh', N'590 Franklyn street', N'a', N'Vancouver', N'New Brunswick', N'Canada', N'B3H2C9', N'254-568-2125', N'Yifan.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (306, N'Lourdes', N'Hamza', N'590 Franklyn street', N'b', N'Torronto', N'Alberta', N'Canada', N'B1H7K7', N'402-578-4893', N'Lourdes.Hamza@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (307, N'Alina', N'Garcia Barrantes', N'590 Franklyn street', N'c', N'Calgary', N'British Columbia', N'Canada', N'B3H2C9', N'254-852-2578', N'Alina.Garcia Barrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (308, N'Yifan', N'Nagamanickam', N'660 Inglis Street', N'd', N'Calgary', N'New Brunswick', N'Canada', N'B1H7K7', N'402-578-4893', N'Yifan.Nagamanickam@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (309, N'Yudhvir', N'Hameed', N'660 Inglis Street', N'a', N'Torronto', N'British Columbia', N'Canada', N'B1H7K7', N'254-852-2578', N'Yudhvir.Hameed@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (310, N'Naveenraj', N'Bhagat', N'890 Quinpool street', N'b', N'Vancouver', N'British Columbia', N'Canada', N'B3H2C9', N'402-578-4893', N'Naveenraj.Bhagat@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (311, N'Jyothi', N'Oliver', N'590 Franklyn street', N'c', N'Vancouver', N'Alberta', N'Canada', N'B3H2C9', N'254-852-2578', N'Jyothi.Oliver@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (312, N'Hanieh', N'Irfanoglu', N'890 Quinpool street', N'd', N'Brampton', N'New Brunswick', N'Canada', N'B9H6J5', N'254-852-2578', N'Hanieh.Irfanoglu@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (313, N'Naghmeh', N'Salvi', N'590 Franklyn street', N'a', N'Brampton', N'New Brunswick', N'Canada', N'B3H2C9', N'254-852-2578', N'Naghmeh.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (314, N'Narendran', N'Bhagat', N'890 Quinpool street', N'b', N'Calgary', N'New Brunswick', N'Canada', N'B9H6J5', N'254-568-2125', N'Narendran.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (315, N'Alina', N'Govindaraj', N'590 Franklyn street', N'c', N'Brampton', N'Alberta', N'Canada', N'B9H6J5', N'254-568-2125', N'Alina.Govindaraj@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (316, N'Hanieh', N'Hameed', N'660 Inglis Street', N'd', N'Brampton', N'New Brunswick', N'Canada', N'B9H6J5', N'254-852-2578', N'Hanieh.Hameed@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (317, N'Ameer', N'Maruf', N'890 Quinpool street', N'a', N'Calgary', N'New Brunswick', N'Canada', N'B9H6J5', N'254-568-2125', N'Ameer.Maruf@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (318, N'Yudhvir', N'Suryawanshi', N'660 Inglis Street', N'b', N'Brampton', N'British Columbia', N'Canada', N'B1H7K7', N'254-568-2125', N'Yudhvir.Suryawanshi@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (319, N'Yudhvir', N'Maruf', N'660 Inglis Street', N'c', N'Calgary', N'British Columbia', N'Canada', N'B9H6J5', N'402-578-4893', N'Yudhvir.Maruf@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (320, N'Yifan', N'Kumar', N'590 Franklyn street', N'd', N'Calgary', N'Alberta', N'Canada', N'B3H2C9', N'254-568-2125', N'Yifan.Kumar@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (321, N'Hanieh', N'Hamza', N'890 Quinpool street', N'a', N'Brampton', N'New Brunswick', N'Canada', N'B1H7K7', N'254-852-2578', N'Hanieh.Hamza@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (322, N'Naghmeh', N'Arantes', N'660 Inglis Street', N'b', N'Vancouver', N'New Brunswick', N'Canada', N'B3H2C9', N'254-852-2578', N'Naghmeh.Arantes@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (323, N'Dinesh Kumar', N'Omar', N'590 Franklyn street', N'c', N'Calgary', N'Alberta', N'Canada', N'B9H6J5', N'254-568-2125', N'Dinesh Kumar.Omar@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (324, N'Jyothi', N'Govindaraj', N'660 Inglis Street', N'd', N'Torronto', N'New Brunswick', N'Canada', N'B9H6J5', N'402-578-4893', N'Jyothi.Govindaraj@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (325, N'Jinal', N'Irfanoglu', N'660 Inglis Street', N'a', N'Vancouver', N'British Columbia', N'Canada', N'B1H7K7', N'254-568-2125', N'Jinal.Irfanoglu@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (326, N'Abhiraj', N'Yin', N'890 Quinpool street', N'b', N'Vancouver', N'New Brunswick', N'Canada', N'B1H7K7', N'254-852-2578', N'Abhiraj.Yin@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (327, N'Hanieh', N'Maruf', N'890 Quinpool street', N'c', N'Torronto', N'New Brunswick', N'Canada', N'B3H2C9', N'254-568-2125', N'Hanieh.Maruf@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (328, N'Caner Adil', N'Suryawanshi', N'590 Franklyn street', N'd', N'Torronto', N'Alberta', N'Canada', N'B3H2C9', N'402-578-4893', N'Caner Adil.Suryawanshi@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (329, N'Yudhvir', N'Singh', N'890 Quinpool street', N'a', N'Calgary', N'British Columbia', N'Canada', N'B1H7K7', N'402-578-4893', N'Yudhvir.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (330, N'First Name', N'Last Name', N'Street', N'b', N'City', N'Province', N'Country', N'Postal Code', N'Phone', N'Email')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (331, N'Jinal', N'Maruf', N'660 Inglis Street', N'c', N'Vancouver', N'British Columbia', N'Canada', N'B1H7K7', N'254-568-2125', N'Jinal.Maruf@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (332, N'Daune', N'Ibragimova', N'660 Inglis Street', N'd', N'Calgary', N'New Brunswick', N'Canada', N'B1H7K7', N'402-578-4893', N'Daune.Ibragimova@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (333, N'Dinesh Kumar', N'Bhagat', N'590 Franklyn street', N'a', N'Torronto', N'Alberta', N'Canada', N'B1H7K7', N'254-852-2578', N'Dinesh Kumar.Bhagat@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (334, N'Yifan', N'Ibragimova', N'660 Inglis Street', N'b', N'Calgary', N'New Brunswick', N'Canada', N'B3H2C9', N'402-578-4893', N'Yifan.Ibragimova@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (335, N'Md. Wahid Tausif', N'Maruf', N'660 Inglis Street', N'c', N'Brampton', N'British Columbia', N'Canada', N'B9H6J5', N'402-578-4893', N'Md. Wahid Tausif.Maruf@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (336, N'Yudhvir', N'Vechoor Padmanabhan', N'660 Inglis Street', N'd', N'Torronto', N'New Brunswick', N'Canada', N'B9H6J5', N'254-568-2125', N'Yudhvir.Vechoor Padmanabhan@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (337, N'Daune', N'Maruf', N'890 Quinpool street', N'a', N'Vancouver', N'New Brunswick', N'Canada', N'B1H7K7', N'254-852-2578', N'Daune.Maruf@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (338, N'Olugbenro', N'Taneja', N'590 Franklyn street', N'b', N'Calgary', N'British Columbia', N'Canada', N'B9H6J5', N'402-578-4893', N'Olugbenro.Taneja@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (339, N'Yudhvir', N'Taneja', N'890 Quinpool street', N'c', N'Calgary', N'New Brunswick', N'Canada', N'B1H7K7', N'254-852-2578', N'Yudhvir.Taneja@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (340, N'Amrit', N'Oliver', N'590 Franklyn street', N'd', N'Calgary', N'Alberta', N'Canada', N'B9H6J5', N'254-568-2125', N'Amrit.Oliver@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (341, N'Sankalp', N'Vechoor Padmanabhan', N'590 Franklyn street', N'a', N'Vancouver', N'British Columbia', N'Canada', N'B9H6J5', N'402-578-4893', N'Sankalp.Vechoor Padmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (342, N'Rodolfo', N'Taneja', N'890 Quinpool street', N'b', N'Brampton', N'Alberta', N'Canada', N'B9H6J5', N'402-578-4893', N'Rodolfo.Taneja@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (343, N'Naghmeh', N'Garcia Barrantes', N'890 Quinpool street', N'c', N'Vancouver', N'New Brunswick', N'Canada', N'B3H2C9', N'254-852-2578', N'Naghmeh.Garcia Barrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (344, N'Jinal', N'Suryawanshi', N'590 Franklyn street', N'd', N'Brampton', N'British Columbia', N'Canada', N'B1H7K7', N'254-852-2578', N'Jinal.Suryawanshi@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (345, N'Ranjit', N'Singh', N'590 Franklyn street', N'a', N'Vancouver', N'British Columbia', N'Canada', N'B3H2C9', N'402-578-4893', N'Ranjit.Singh@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (346, N'Ranjit', N'Hameed', N'660 Inglis Street', N'b', N'Torronto', N'Alberta', N'Canada', N'B1H7K7', N'402-578-4893', N'Ranjit.Hameed@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (347, N'Wasiq Bin', N'QasemiBoroujeni', N'890 Quinpool street', N'c', N'Brampton', N'British Columbia', N'Canada', N'B9H6J5', N'254-568-2125', N'Wasiq Bin.QasemiBoroujeni@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (348, N'Caner Adil', N'QasemiBoroujeni', N'590 Franklyn street', N'd', N'Calgary', N'Alberta', N'Canada', N'B3H2C9', N'254-852-2578', N'Caner Adil.QasemiBoroujeni@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (349, N'Wasiq Bin', N'Yin', N'890 Quinpool street', N'a', N'Brampton', N'New Brunswick', N'Canada', N'B1H7K7', N'402-578-4893', N'Wasiq Bin.Yin@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (350, N'Vivek', N'Suryawanshi', N'660 Inglis Street', N'b', N'Vancouver', N'Alberta', N'Canada', N'B9H6J5', N'402-578-4893', N'Vivek.Suryawanshi@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (351, N'Jinal', N'Govindaraj', N'890 Quinpool street', N'c', N'Brampton', N'Alberta', N'Canada', N'B9H6J5', N'254-852-2578', N'Jinal.Govindaraj@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (352, N'Yudhvir', N'Singh', N'660 Inglis Street', N'd', N'Calgary', N'New Brunswick', N'Canada', N'B9H6J5', N'254-568-2125', N'Yudhvir.Singh@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (353, N'Wasiq Bin', N'Hameed', N'890 Quinpool street', N'a', N'Calgary', N'New Brunswick', N'Canada', N'B1H7K7', N'254-568-2125', N'Wasiq Bin.Hameed@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (354, N'Ranjit', N'Arantes', N'590 Franklyn street', N'b', N'Torronto', N'British Columbia', N'Canada', N'B3H2C9', N'402-578-4893', N'Ranjit.Arantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (355, N'Karthik', N'Taneja', N'660 Inglis Street', N'c', N'Vancouver', N'New Brunswick', N'Canada', N'B3H2C9', N'402-578-4893', N'Karthik.Taneja@hotmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (356, N'Naghmeh', N'Singh', N'660 Inglis Street', N'd', N'Torronto', N'New Brunswick', N'Canada', N'B9H6J5', N'402-578-4893', N'Naghmeh.Singh@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (357, N'Florisha', N'Garcia Barrantes', N'590 Franklyn street', N'a', N'Vancouver', N'Alberta', N'Canada', N'B9H6J5', N'254-852-2578', N'Florisha.Garcia Barrantes@gmail.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (358, N'Abhiraj', N'Irfanoglu', N'660 Inglis Street', N'b', N'Calgary', N'British Columbia', N'Canada', N'B9H6J5', N'254-852-2578', N'Abhiraj.Irfanoglu@rediff.com')
INSERT [dbo].[Customer] ([id], [firstName], [lastName], [streetNumber], [Address], [city], [province], [country], [postalCode], [phoneNumber], [emailAddress]) VALUES (359, N'Mohannad', N'Bhagat', N'590 Franklyn street', N'c', N'Vancouver', N'British Columbia', N'Canada', N'B9H6J5', N'254-852-2578', N'Mohannad.Bhagat@hotmail.com')
SET IDENTITY_INSERT [dbo].[Customer] OFF
USE [master]
GO
ALTER DATABASE [Customer] SET  READ_WRITE 
GO
