namespace Assignment2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CustomerDataModel : DbContext
    {
        public CustomerDataModel()
            : base("name=CustomerDataModel")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.firstName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.lastName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.streetNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.province)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.postalCode)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.phoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.emailAddress)
                .IsUnicode(false);
        }
    }
}
