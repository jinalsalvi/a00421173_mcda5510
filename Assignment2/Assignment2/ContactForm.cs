﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.Entity;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;

namespace Assignment2
{
 
    public partial class ContactForm : Form
    {
        int maxIndex = 0;

        public ContactForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataCount = allData.Count();
            if (index > 0)
            {
                index = index - 1;

                UpdateData(index);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataCount = allData.Count();
            if (index < dataCount-1)
            {
                index = index + 1;

                UpdateData(index);
            }
        }
        int index = 0;
        Dictionary<int, Customer> allData = new Dictionary<int, Customer>();
        int dataCount = 0;




        private void ContactForm_Load(object sender, EventArgs e)
        {
            allData.Clear();
            int index = 0;
          
            using (CustomerDataModel db = new CustomerDataModel())
            {
                DbSet<Customer> customers = db.Customers;

                foreach (Customer customer in customers)
                {
                    allData.Add(index, customer);
                    
                    index++;
                    maxIndex++;
                }
                UpdateData(0);
            }
        }
        private void UpdateData(int index)
        {
            Customer personData = null;
            if (allData.TryGetValue(index, out personData))
            {
                this.textBoxFN.Text = personData.firstName;
                if (personData.firstName.Length > 50)
                {
                    this.textBoxError.Text = "First Name length is more than 50 charactors";
                }
                this.textBoxLN.Text = personData.lastName;

                if (personData.lastName.Length > 50)
                {
                    this.textBoxError.Text = "Last Name length is more than 50 charactors";
                }

                this.textBoxSN.Text = personData.streetNumber;

                if (!Regex.IsMatch(personData.streetNumber, @"^[a-zA-Z]+$"))
                {
                    this.textBoxError.Text = "Street Number can not be charctor";
                }

                this.textBoxAddress.Text = personData.Address;

                if (personData.Address.Length > 50)
                {
                    this.textBoxError.Text = "Address length is more than 50 charactors";
                }

                this.textBoxC.Text = personData.city;

                if (personData.city.Length > 50)
                {
                    this.textBoxError.Text = "City length is more than 50 charactors";
                }

                this.textBoxCOUNTRY.Text = personData.country;

                if (personData.firstName.Length > 50)
                {
                    this.textBoxError.Text = "First Name length is more than 50 charactors";
                }

                this.textBoxP.Text = personData.province;

                if (personData.province.Length > 50)
                {
                    this.textBoxError.Text = "Provience length is more than 50 charactors";
                }

                this.textBoxPN.Text = personData.phoneNumber;

              
                this.textBoxEA.Text = personData.emailAddress;
              
                this.textBoxPC.Text = personData.postalCode;

                if (personData.postalCode.Length != 7)
                {
                    this.labelError.Text = "Postal code length must be 7 (6 plus space)";
                }

                this.textBoxID.Text = personData.id.ToString();

            }
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                textImportFile.Text = openFileDialog1.FileName;
            }
        }

        private void ButtonImport_Click(object sender, EventArgs e)
        {
            int lineNum = 1;
            String file = null;
            if (this.textImportFile.Text != "")
            {
                file = this.textImportFile.Text;// @"C: \Users\Jinal\Desktop\customers.csv";
            //String file = textImportFile.Text;
            using (TextFieldParser parser = new TextFieldParser(file))
            {
                parser.Delimiters = new string[] { "," };
                    while (true)
                    {
                            string[] parts = parser.ReadFields();
                            if (parts == null)
                            {
                                break;
                            }
                            using (CustomerDataModel db = new CustomerDataModel())
                            {
                            if (lineNum != 1)
                            {
                               
                                DbSet<Customer> customers = db.Customers;
                                Customer cust = new Customer();//
                                cust.firstName = parts[0];
                                cust.lastName = parts[1];
                                cust.streetNumber = parts[2];

                                cust.Address = parts[3];
                                cust.city = parts[4];
                                cust.province = parts[5];

                                cust.country = parts[6];
                                cust.postalCode = parts[7];
                                cust.phoneNumber = parts[8];
                                cust.emailAddress = parts[9];




                                // this saves it in the DB to be saved when Save is called
                                customers.Add(cust);
                                db.SaveChanges();
                                allData.Add(maxIndex, cust);
                                maxIndex++;
                            }
                            lineNum++;

                        }
                    }
            }
            UpdateData(index);
            }

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelError_Click(object sender, EventArgs e)
        {

        }
    }
}
